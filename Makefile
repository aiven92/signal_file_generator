Q=
LD=$(CC)

LDFLAGS += $(EXTRALDFLAGS) -lm

.PHONY: all
all: si_gen

SI_GEN_SRC = \
	si_gen.c

SI_GEN_O = $(SI_GEN_SRC:.c=.o)

si_gen: $(SI_GEN_O)
	$(Q) $(LD) $^ -o $@ $(LDFLAGS)

.PHONY: clean
clean:
	rm -rfv si_gen
	rm -rfv $(SI_GEN_O)
