#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

enum mode_e{
  BINARY,
  COMPLEMENT
};

// размер в байтах включая размер - 4 байта
// частота в герцах - 4 байта
//
// канал_1 значение - 2 байта
// ...
// канал_n значение - 2 байта
// = 2 * n байт
//
//
// итоговый размер 2 * n * количество отсчётов + 8 байт
//
//
// напряжение = значение * 2 * 2.5 * 1/32768

static void print_usage(void)
{
  printf("Binary signals file generator\nUsage:\n");
  printf("\t-f\t Common frequency of harmonic functions\n");
  printf("\t-n\t Number of samples in one period \n");
  printf("\t-c\t Number of output channels\n");
  printf("\t-o\t Optional output file name\n");

  printf("\n");
}

double gen_function(double value)
{
  return sin(value);
}

int16_t double_to_int(double value)
{
  return (int16_t)(value * 32768 / 5);
}

int main(int argc, char *argv[])
{
  uint32_t frequence = 0;
  uint32_t counts = 0;
  uint32_t channels = 0;
  enum mode_e mode = COMPLEMENT;

  char* file_name = NULL;
  int fd;

  bool debug = false;
  int opt;
  while ((opt = getopt(argc, argv, "f:n:c:sudo:")) != -1)
    {
      switch (opt)
        {
          case 'o':
            file_name = optarg;
            break;

          case 'f':
            frequence = atoi(optarg);
            break;

          case 'n':
            counts = atoi(optarg);
            break;

          case 'c':
            channels = atoi(optarg);
            break;

          case 'd':
            debug = true;
            break;

          case 's': mode = COMPLEMENT;  break;
          case 'u': mode = BINARY;      break;

          default:
              print_usage();
              exit(EXIT_FAILURE);
        }
    }

  if(!(frequence && counts && channels))
    {
      print_usage();
      exit(EXIT_FAILURE);
    }

  printf("frequence\t%dHz\n", frequence);
  printf("counts\t%d\n", counts);
  printf("channels\t%d\n", channels);
  printf("mode\t%s\n", mode==COMPLEMENT ? "COMPLEMENT" : "BINARY");
  printf("period\t%dus\n", 1000000/(frequence * counts));

  if(file_name)
    {
      fd = open(file_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
      if(fd < 0)
        {
          printf("can't open file %d", errno);
          exit(EXIT_FAILURE);
        }
    }

  uint32_t size = 2 * channels * counts + 8;
  printf("size\t%d [0x%08X]\n", size, size);

  if(file_name)
    {
      write(fd, &size , sizeof(size));
      write(fd, &frequence, sizeof(frequence));
    }

  int16_t* buff = calloc(channels, sizeof(int16_t));
  if(!buff)
    {
      printf("can't allocate buffer %d", errno);
      close(fd);
      exit(EXIT_FAILURE);
    }

  for(uint32_t count = 0; count < counts; count++)
    {
      for(uint32_t channel = 0; channel < channels; channel++)
        {
          double stamp = count * 2 * M_PI / counts;
          buff[channel] = double_to_int(gen_function(stamp));

          if(debug)
            {
              printf("%d.%d\t%f [0x%04X]\n", channel, count,
                     gen_function(stamp), buff[channel]);
            }
        }
      if(file_name)
        {
          write(fd, buff, sizeof(int16_t)*channels);
        }
    }

  close(fd);
}
